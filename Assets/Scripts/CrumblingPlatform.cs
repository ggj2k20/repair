﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

using UnityEngine.U2D;

public class CrumblingPlatform : MonoBehaviour
{
    [SerializeField]
    protected float crumbleDelay;

    [SerializeField]
    protected SpriteShapeController shapeController;

    [SerializeField]
    protected float gap;

    [SerializeField]
    protected float minWidth;

    [ContextMenu("Crumble")]
    public async void Crumble() {
        if (this.crumbleDelay > 0) {
            await Task.Delay(Mathf.CeilToInt(this.crumbleDelay * 1000.0f));
            if (this == null) return;
        }
        Spline spline = this.shapeController.spline;
        int pointCount = spline.GetPointCount();
        for (int i = 0; i < pointCount - 1; i++) {
            GameObject child = Instantiate(this.gameObject);

            Destroy(child.GetComponent<CrumblingPlatform>());
            Destroy(child.GetComponent<Collider2D>());

            SpriteShapeController childController = child.GetComponent<SpriteShapeController>();
            Spline childSpline = childController.spline;

            childSpline.Clear();

            Vector3 point0 = spline.GetPosition(i);
            Vector3 point1 = spline.GetPosition(i+1);

            if (i > 0) {
                point0 = Vector3.MoveTowards(point0, point1, this.gap / 2.0f);
            }
            if (i < pointCount - 1)
            {
                point1 = Vector3.MoveTowards(point1, point0, this.gap / 2.0f);
            }

            float distance = Vector3.Distance(point0, point1);
            if (distance < this.minWidth) {
                point0 = spline.GetPosition(i);
                point1 = spline.GetPosition(i + 1);
                Vector3 center = (point0 + point1) / 2.0f;
                point0 = Vector3.MoveTowards(center, point0, this.minWidth / 2.0f);
                point1 = Vector3.MoveTowards(center, point1, this.minWidth / 2.0f);
            }

            childSpline.InsertPointAt(0, point0);
            childSpline.InsertPointAt(1, point1);

            childController.RefreshSpriteShape();

            //child.AddComponent<BoxCollider2D>();
            Rigidbody2D rigidbody = child.AddComponent<Rigidbody2D>();
            rigidbody.velocity = Random.insideUnitCircle;
        }
        Destroy(this.gameObject);
    }
}
