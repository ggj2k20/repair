﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedDestruction : MonoBehaviour {
	[SerializeField]
	protected float delay;

    public void StartDestruction() {
        if (this.delay == 0) {
            Destroy(this.gameObject);
        } else {
            StartCoroutine(this.DestructionRoutine());
        }
    }

    protected IEnumerator DestructionRoutine() {
        DestructionEffect[] destructionEffects = this.GetComponentsInChildren<DestructionEffect>();
        if (destructionEffects == null || destructionEffects.Length == 0) {
            yield return new WaitForSeconds(this.delay);
        } else {
            float delta = 0;
            while (delta < this.delay)
            {
                foreach (DestructionEffect effect in destructionEffects)
                {
                    effect.UpdateEffect(delta / this.delay);
                }
                yield return null;
                delta += Time.deltaTime;
            }
        }
        Destroy(this.gameObject);
    }
}
