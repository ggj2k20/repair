﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using Utilities;

public class Main : MonoBehaviour
{

    public UnityEvent OnPlayerGoal = new UnityEvent();

    public SceneField gameScene;

    public bool BlockInput { get; set; }
    public bool FreezePlayer { get; set; }

    protected static Main instance;
    public static Main Instance => Main.instance;
    void Awake()
    {
        if (instance != null) {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        SceneManager.LoadSceneAsync(gameScene);
    }

    public void Close() {
        Debug.Log("Main.Close");
        Application.Quit();
    }
}
