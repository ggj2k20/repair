﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField]
    Vector3 axis;

    [SerializeField]
    float distance;

    [SerializeField]
    float speed;

    [SerializeField]
    AnimationCurve curve;

    [SerializeField]
    bool moveOnlyWithPlayer;

    Vector3 startPos;
    float t;
    float d;

    private void Awake()
    {
        startPos = this.transform.localPosition;
    }

    private void Update()
    {
        if (!moveOnlyWithPlayer) {
            this.DoMove();
        }
    }

    public void UpdateWithPlayer() {
        if (moveOnlyWithPlayer) {
            this.DoMove();
        }
    }

    void DoMove() {
        t += Time.deltaTime * this.speed;
        d = Mathf.PingPong(t, this.distance) / this.distance;
        this.transform.localPosition = startPos + axis * distance * this.curve.Evaluate(d);
    }
}
