﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour {

    [SerializeField]
    protected UnityEvent onGrounded = new UnityEvent();

    [SerializeField]
	protected PlayerController controller;
	[SerializeField]
	protected Rigidbody2D rigidbody;
	[SerializeField]
	protected float speed;
	[SerializeField]
	protected float jump;
	[SerializeField]
	protected float breakForce;

	[SerializeField]
	protected Collider2D collider;

	[SerializeField]
	protected LayerMask groundingLayers;

	[SerializeField]
	protected bool grounded;

	protected ContactPoint2D[] contacts;
	public ContactPoint2D[] Contacts => this.contacts;
	protected int contactCount;
	public int ContactCount => this.contactCount;


	[SerializeField]
	protected List<ActivePowerup> activePowerups = new List<ActivePowerup>();
	public List<ActivePowerup> ActivePowerups => this.activePowerups;

	void Awake() {
		this.contacts = new ContactPoint2D[10];
	}

	void Update() {
		this.UpdateActivePowerups();
	}

	protected void UpdateActivePowerups() {
		for (int i = 0; i < this.ActivePowerups.Count; i++) {
			ActivePowerup activePowerup = this.ActivePowerups[i];
			activePowerup.Duration += Time.deltaTime;
			if (activePowerup.Duration > activePowerup.Powerup.Duration) {
				this.ActivePowerups[i] = null;
			}
		}
		this.ActivePowerups.RemoveAll(item => item == null);
	}

    void FixedUpdate() {
        if (Main.Instance.FreezePlayer) {
            this.rigidbody.velocity = Vector2.zero;
            this.rigidbody.isKinematic = true;
            return;
        }

        this.rigidbody.isKinematic = false;

        this.CheckGrounding();
		
		float playerSpeed = this.speed;
		
		this.ApplyPowerups(ref playerSpeed);

		Vector2 movement = this.controller.GetMovementControl();
  		this.rigidbody.velocity += movement * playerSpeed * Time.fixedDeltaTime;

		if (this.grounded && Mathf.Approximately(movement.sqrMagnitude, 0)) {
			Vector2 velocity = this.rigidbody.velocity;
			velocity.x *= (1.0f - this.breakForce);
			this.rigidbody.velocity = velocity;
		}

		if (this.grounded && this.controller.GetJump()) {
			this.rigidbody.velocity += Vector2.up * this.jump;
		}

    }

	protected void ApplyPowerups(ref float speed) {
		float speedBonus = 1;
  		foreach (ActivePowerup activePowerup in this.ActivePowerups) {
   			activePowerup.ApplySpeedBonus(ref speedBonus);
		}
		speed *= speedBonus;
	}

	protected void CheckGrounding() {
        bool wasGrounded = this.grounded;
		this.grounded = false;
		this.contactCount = this.collider.GetContacts(this.contacts);
		for (int i = 0; i < this.contactCount; i++) {
			int layer = this.contacts[i].collider.gameObject.layer;
			int layerMask = 1 << layer;
			if ((this.groundingLayers.value & layerMask) == layerMask) {
				this.grounded = true;
			}

			MovingPlatform platform = this.contacts[i].collider.GetComponent<MovingPlatform>();
			if (platform != null) {
				platform.UpdateWithPlayer();
			}
		}
        if (this.grounded && !wasGrounded) {
            this.onGrounded.Invoke();
        }
	}

	public void OnCollisionEnter2D(Collision2D collision) {
  		PowerupContainer powerupContainer = collision.collider.GetComponent<PowerupContainer>();
  		if (powerupContainer != null && !powerupContainer.Consumed) {
   			this.activePowerups.Add(new ActivePowerup() { Powerup = powerupContainer.Powerup });
   			powerupContainer.Consume();
            return;
  		}

        CrumblingPlatform crumblingPlaform = collision.collider.GetComponent<CrumblingPlatform>();
        if (crumblingPlaform != null) {
            crumblingPlaform.Crumble();
            return;
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider);
        if (collider.GetComponent<PlayerGoal>() != null) {
            Main.Instance?.OnPlayerGoal.Invoke();
        }
    }
}
