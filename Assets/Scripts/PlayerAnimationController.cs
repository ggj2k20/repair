﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

[RequireComponent(typeof(SkeletonAnimation)), ExecuteAlways]
public class PlayerAnimationController : MonoBehaviour
{
	[SerializeField]
	protected SkeletonAnimation skeletonAnimation = null;

	[SerializeField]
	protected List<AnimationNamePair> animationNamePairs = new List<AnimationNamePair>();
	[SerializeField]
	protected PlayerController playerController;

	public Spine.Animation TargetAnimation { get; private set; }

	[System.Serializable]
	public class AnimationNamePair
	{
		public string name;
		public AnimationReferenceAsset animation;
	}

	[ContextMenu("Jump")]
	void Jump()
	{
		PlayOneShot("Jump", 0);
	}

	[ContextMenu("Run")]
	void Run()
	{
		PlayAnimation("Run", 0);
	}

	protected bool flipper = false;
	[ContextMenu("Flip")]
	void Flip()
	{
		flipper = !flipper;
		FaceRight(flipper);
	}

	private void Awake()
	{
		if (skeletonAnimation == null)
		{
			skeletonAnimation = GetComponent<SkeletonAnimation>();
		}
		if (playerController == null)
		{
			playerController = GetComponent<PlayerController>();
		}

		foreach (var pair in animationNamePairs)
		{
			pair.animation.Initialize();
		}
	}

	public void SetGrounded()
	{
		isGrounded = true;
	}
	enum AnimationState
	{
		Jump, Run, Idle
	}
	private bool isGrounded = true;
	AnimationState state = AnimationState.Idle;
	private void Update()
	{

		if (skeletonAnimation == null)
		{
			skeletonAnimation = GetComponent<SkeletonAnimation>();
		}

		Vector2 movement = this.playerController.GetMovementControl();
		bool isJump = this.playerController.GetJump();

		if(isJump && isGrounded)
		{
			isGrounded = false;
			if (state != AnimationState.Jump)
			{
				state = AnimationState.Jump;
				PlayAnimation("Jump", 0);
			}
		}
		else if(!Mathf.Approximately(movement.sqrMagnitude, Mathf.Epsilon))
		{
			if (state != AnimationState.Run && isGrounded)
			{
				state = AnimationState.Run;
				PlayAnimation("Run", 0);

			}

			if(movement.x > 0)
			{
				FaceRight(true);
			} else
			{
				FaceRight(false);
			}

		} else
		{
			if (state != AnimationState.Idle && isGrounded)
			{
				state = AnimationState.Idle;
				PlayAnimation("Idle", 0);
			}
		}
	}

	/// <summary> Set facing direction, true = right, false = left. </summary>
	public void FaceRight(bool faceRight)
	{
		skeletonAnimation.Skeleton.ScaleX = faceRight ? 1 : -1;
	}

	/// <summary> Set new Animation to play on loop, use PlayOneShot to play without looping </summary>
	public void PlayAnimation(string animationName, int layerIndex)
	{
		PlayAnimation(Animator.StringToHash(animationName), layerIndex);
	}

	/// <summary> Set new Animation to play on loop, use PlayOneShot to play without looping </summary>
	public void PlayAnimation(int nameHash, int layerindex)
	{
		var foundAnimation = GetAnimationByHash(nameHash);
		if(foundAnimation == null)
		{
			return;
		}

		PlayAnimation(foundAnimation, layerindex);
	}

	/// <summary> Get Animation by its hashed name </summary>
	public Spine.Animation GetAnimationByHash(int hash)
	{
		var foundAnimation = animationNamePairs.Find(entry => Animator.StringToHash(entry.name) == hash);
		return (foundAnimation == null) ? null : foundAnimation.animation;
	}

	/// <summary> Set new Animation to play on loop, use PlayOneShot to play without looping </summary>
	public void PlayAnimation(Spine.Animation targetAnimation, int layerIndex)
	{
		this.TargetAnimation = targetAnimation;
		skeletonAnimation.AnimationState.SetAnimation(layerIndex, targetAnimation, true);
	}

	/// <summary> Play animation without looping it, continue previous animation after done </summary>
	public void PlayOneShot(string animationName, int layerIndex)
	{
		PlayOneShot(Animator.StringToHash(animationName), layerIndex);
	}

	/// <summary> Play animation without looping it, continue previous animation after done </summary>
	public void PlayOneShot(int hash, int layerIndex)
	{
		var foundAnimation = GetAnimationByHash(hash);
		if(foundAnimation == null)
		{
			return;
		}

		PlayOneShot(foundAnimation, layerIndex);
	}

	/// <summary> Play animation without looping it, continue previous animation after done </summary>
	public void PlayOneShot(Spine.Animation oneShot, int layerIndex)
	{
		var state = skeletonAnimation.AnimationState;
		state.SetAnimation(layerIndex, oneShot, false);

		state.AddAnimation(layerIndex, this.TargetAnimation, true, 0f);
	}

	public void PlayFumble()
	{
		var state = skeletonAnimation.AnimationState;
		state.SetAnimation(0, "Fumble", false);

		state.AddAnimation(0, this.TargetAnimation, true, 0f);
	}
}
