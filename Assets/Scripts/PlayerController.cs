﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	
	[SerializeField]
	protected KeyCode keyUp;
	[SerializeField]
	protected KeyCode keyLeft;
	[SerializeField]
	protected KeyCode keyRight;
	[SerializeField]
	protected KeyCode keyDown;
	[SerializeField]
	protected KeyCode keyJump;

	public Vector2 GetMovementControl() {
		Vector2 movementControl = new Vector2();
        if (true)
        {
            if (Input.GetKey(this.keyUp))
            {
                movementControl.y += 1;
            }
            if (Input.GetKey(this.keyLeft))
            {
                movementControl.x -= 1;
            }
            if (Input.GetKey(this.keyRight))
            {
                movementControl.x += 1;
            }
            if (Input.GetKey(this.keyDown))
            {
                movementControl.y -= 1;
            }
        }
		return movementControl;
	}

	public bool GetJump() {
		return Input.GetKey(this.keyJump);
	}
}
