﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
	[SerializeField] protected float MovementSpeed = 2f;
	[SerializeField] protected float jumpHeight = 5f;
	[Range(0, .3f)] [SerializeField] protected float movementSmoothing = .05f;
	[SerializeField] protected LayerMask groundLayer;
	[SerializeField] protected Transform groundCheck;
	[SerializeField] protected PlayerController playerController;
	[SerializeField] protected float fallingBoost = 1f;
	[SerializeField] protected PlayerAnimationController pac;

	const float groundCheckRadius = .5f;
	protected bool isGrounded = true;
	protected Rigidbody2D rigidBody;
	protected bool faceingRight = true;
	protected Vector2 velocity = Vector2.zero;

	public UnityEvent OnLandEvent;

	private void Awake()
	{
		rigidBody = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

	}

	private void Update()
	{
		Move(playerController.GetMovementControl().x * MovementSpeed, playerController.GetJump());

		if(!isGrounded)
		{
			if(rigidBody.velocity.y > 0)
			{

			}
		}
	}

	private void FixedUpdate()
	{
		bool wasGrounded = isGrounded;
		isGrounded = false;

		Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundCheckRadius, groundLayer);
		for (int i = 0; i < colliders.Length; ++i)
		{
			if (colliders[i].gameObject != gameObject)
			{
				isGrounded = true;
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}
	}

	public void Move(float move, bool jump)
	{
		Vector2 targetVelocity = new Vector2(move * 10f, rigidBody.velocity.y);
		rigidBody.velocity = Vector2.SmoothDamp(rigidBody.velocity, targetVelocity, ref velocity, movementSmoothing);

		if (isGrounded && jump)
		{
			pac.PlayOneShot("Jump", 0);
			isGrounded = false;
			rigidBody.velocity = (new Vector2(rigidBody.velocity.x, Mathf.Sqrt(-2.0f * Physics2D.gravity.y * jumpHeight)));
		} else if(!isGrounded && !jump && rigidBody.velocity.y > 0)
		{
			Debug.Log("damping");
			pac.PlayOneShot("JumpMiddle", 0);
			rigidBody.AddForce(new Vector2(0f, -fallingBoost));
		} else
		{
			pac.PlayAnimation("jumpEnd", 0);
		}
	}
}
