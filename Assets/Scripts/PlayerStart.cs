﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStart : MonoBehaviour
{
    [SerializeField]
    protected GameObject playerPrefab;

    void Start()
    {
        GameObject player = Instantiate(this.playerPrefab.gameObject);
        player.transform.position = this.transform.position;
    }
}
