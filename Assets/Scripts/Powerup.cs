﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName="Powerup", fileName="Powerup", order=0)]
public class Powerup : ScriptableObject {

	[SerializeField]
	protected float duration;
	public float Duration => this.duration;
    
	[SerializeField]
	protected float speedBonus;
	public float SpeedBonus => this.speedBonus;

	[SerializeField]
	protected AnimationCurve speedBonusCurve;
	public AnimationCurve SpeedBonusCurve => this.speedBonusCurve;

}

[System.Serializable]
public class ActivePowerup {
	public Powerup Powerup;
	public float Duration;
	
	public void ApplySpeedBonus(ref float speedBonus) {
  		float delta = this.Duration / this.Powerup.Duration;
  		speedBonus *= this.Powerup.SpeedBonusCurve.Evaluate(delta) * this.Powerup.SpeedBonus;
	}
}
