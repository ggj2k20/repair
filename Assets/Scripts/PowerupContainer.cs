﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerupContainer : MonoBehaviour {

    [SerializeField]
    protected UnityEvent onConsumed = new UnityEvent();
    
 	[SerializeField]
 	protected Powerup powerup;
 	public Powerup Powerup => this.powerup;

 	protected bool consumed;
 	public bool Consumed => this.consumed;



 	public void Consume() {
  		this.consumed = true;
        this.onConsumed.Invoke();
    }
}
