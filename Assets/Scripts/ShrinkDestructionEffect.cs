﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkDestructionEffect : DestructionEffect {

    [SerializeField]
    protected AnimationCurve scaleCurve;

    protected Vector3 localScale;

    private void Awake()
    {
        this.localScale = this.transform.localScale;
    }

    public override void UpdateEffect(float delta) {
        float scale = this.scaleCurve.Evaluate(delta);
        this.transform.localScale = this.localScale * scale;
    }
}
